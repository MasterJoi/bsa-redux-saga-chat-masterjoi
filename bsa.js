import Chat from './src/chat/Chat';
import rootReducer from './src/store'

export default {
    Chat,
    rootReducer,
};