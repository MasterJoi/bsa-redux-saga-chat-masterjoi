import { combineReducers } from "redux";
import chat from "../chat/reducer"
import editMessageModal from "../editMessageModal/reducer"
import login from "../login/reducer"
import users from "../users/reducer"
import userPage from "../userPage/reducer"


const rootReducer = combineReducers({
    chat,
    editMessageModal,
    login,
    users,
    userPage
});

export default rootReducer;