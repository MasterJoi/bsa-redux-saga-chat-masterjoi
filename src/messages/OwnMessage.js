import * as React from "react";
import Button from "react-bootstrap/Button";
import Avatar from 'react-avatar';
import { TrashFill, Pencil } from 'react-bootstrap-icons';


const CardFooter = {
    width: "100%",
    display: "flex",
    alignItems: "center",
}

const CardFotterChildStyle = {
    flex: "1",
}

const OwnMessage = ({ item, isLast, onEdit, onDelete, date }) => {
    return (
        <li className="message list-group-item" key={item.userId} style={{ alignSelf: "flex-end", margin: "10px", border: "1px solid #1d1f1c", borderRadius: "30px" }}>
            <Avatar className='message-user-avatar' style={{ marginRight: "10px" }} size="50" src={item.avatar} round={true} />
            <strong className='message-user-name'>{item.user}</strong>
            <p className='message-text'>{item.text}</p>
            <div style={CardFooter}>
                <div style={CardFotterChildStyle} >
                    {
                        <Button className='message-delete' variant="dark" onClick={() => { onDelete(item.id) }} style={{ margin: "5px" }}>
                            <TrashFill />
                        </Button>
                    }
                    {
                        <Button style={{ margin: "5px", display: isLast ? 'inline-block' : 'none' }} className='message-edit' variant="dark" onClick={() => { onEdit(item.id) }}>
                            <Pencil />
                        </Button>
                    }
                </div>
                <footer className="blockquote-footer" style={{ alignSelf: 'flex-end' }}>
                    <cite className='message-time'>{date}</cite>
                </footer>
            </div>
        </li>
    )
}

export default OwnMessage;
