import React, { Component } from "react";
import { connect } from 'react-redux';
import UserItem from './UserItem';
import * as actions from './actions';
import PropTypes from 'prop-types';
import Preloader from "../preloader/Preloader";
import { Container, ListGroup, Button, Alert } from "react-bootstrap";

const UserListStyle = {
    position: 'absolute',
    left: '-1',
    right: '0',
    display: 'flex',
    justifyContent: 'space-evenly',
    overflow: 'auto'
}

class UserList extends Component {
    constructor(props) {
        super(props);
        this.onEdit = this.onEdit.bind(this);
        this.onDelete = this.onDelete.bind(this);
        this.onAdd = this.onAdd.bind(this);
    }

    componentDidMount() {
        this.props.fetchUsers();
    }

    onEdit(id) {
        this.props.history.push(`/adminPage/user/${id}`);
    }

    onDelete(id) {
        this.props.deleteUser(id);
    }

    onAdd() {
        this.props.history.push('/adminPage/user');
    }

    render() {
        return this.props.currUserType === "admin" ? (this.props.isLoading ? <Preloader /> :
            <Container style={UserListStyle}>
                <ListGroup variant="flush" style={{ flexGrow: '1'}}> {
                    this.props.users.map(user => {
                        return (
                            <ListGroup.Item>
                                <UserItem
                                    key={user.id}
                                    id={user.id}
                                    name={user.name}
                                    surname={user.surname}
                                    email={user.email}
                                    onEdit={this.onEdit}
                                    onDelete={this.onDelete}
                                />
                            </ListGroup.Item>
                        );
                    },
                    <ListGroup.Item>
                        <Button variant="success"
                            onClick={this.onAdd}
                            style={{ margin: "5px" }}
                        >
                            Add user
                        </Button>
                    </ListGroup.Item>)
                }
                </ListGroup>
                <div className="col-2 m-1">
                    <button
                        className="btn btn-success"
                        onClick={this.onAdd}
                        style={{ margin: "5px" }}
                    >
                        Add user
                    </button>
                </div>
            </Container>) : <Alert variant="danger">
            <Alert.Heading>Access is denied!</Alert.Heading>
            <p>
                You need to have administrator rights to get to this page!
            </p>
        </Alert>
    }
}

UserList.propTypes = {
    userData: PropTypes.object
};

const mapStateToProps = (state) => {
    return {
        users: state.users.users,
        isLoading: state.users.isLoading,
        currUserType: state.login.userType
    }
};

const mapDispatchToProps = {
    ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(UserList);