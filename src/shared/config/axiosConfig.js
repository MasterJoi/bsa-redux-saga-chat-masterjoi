const instance = {
    baseURL: 'http://localhost:3001',
    headers: {
        'authorization': 'admin',
        'Content-Type': 'application/json;charset=UTF-8',
        "Access-Control-Allow-Origin": "*",
    }
};

export default instance;