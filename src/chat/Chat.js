import * as React from "react";
import Header from "../header/Header";
import MessageList from "../messageList/MessageList";
import MessageInput from "../messageInput/MessageInput";
import Preloader from "../preloader/Preloader";
import { connect } from 'react-redux';
import * as actions from './actions';
import { setCurrentMessageId } from '../editMessageModal/actions'



class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.handleSendClick = this.handleSendClick.bind(this);
        this.handleDeleteClick = this.handleDeleteClick.bind(this);
        this.handleLikeMessage = this.handleLikeMessage.bind(this);
        this.handleEditClick = this.handleEditClick.bind(this);
    }

    async componentDidMount() {
        if (!this.props.userName) {
            this.props.history.push(`/`)
        }
        this.props.getMessages();
        window.addEventListener('keydown', (ev) => {
            if (ev.key === 'ArrowUp' && this.props.messages[this.props.messages.length - 1].user === this.props.userName) {
                this.handleEditClick(this.props.messages[this.props.messages.length - 1].id);
            }
        })
    }

    handleLikeMessage(id) {
        this.props.likeMessage(id);
    }

    handleDeleteClick(id) {
        this.props.deleteMessage(id);
    }

    handleEditClick(id) {
        this.props.history.push(`chat/edit/${id}`)
    }

    handleSendClick(newMessage) {
        this.props.sendMessage(newMessage);
    }

    getParticipantsNumber(messages) {
        let userNames = messages.map((elem) => { return elem.user; });
        let uniqueUserNames = userNames.filter((value, index, self) => {
            return self.indexOf(value) === index;
        });

        return uniqueUserNames.length;
    }

    render() {
        
        return (
            this.props.isLoading ?
                <Preloader/> :
            <div className='chat'>
                <Header
                    currentUser={this.props.userName}
                    participantsNumber={this.getParticipantsNumber(this.props.messages)}
                    messagesNumber={this.props.messages.length}
                    lastMessage={this.props.messages[this.props.messages.length - 1].createdAt}
                />
                <MessageList
                    numberOfMessages={this.props.messages.length}
                    messages={this.props.messages}
                    currentUser={this.props.userName}
                    likeMessageHandler={this.handleLikeMessage}
                    deleteMessageHandler={this.handleDeleteClick}
                    editMessageHandler={this.handleEditClick}
                />
                <MessageInput
                    sendHandler={this.handleSendClick}
                    currentUser={this.props.userName}
                />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        userName: state.login.userName,
        messages: state.chat.messages,
        isLoading: state.chat.isLoading
    }
};
const mapDispatchToProps = {
    ...actions,
    setCurrentMessageId
};
export default connect(mapStateToProps, mapDispatchToProps)(Chat);