import React from 'react';
import Chat from './chat/Chat'
import UserList from './users/UserList';
import UserPage from './userPage/UserPage';
import LoginPage from './login/LoginPage';
import { Switch, Route } from 'react-router-dom';
import EditMessageModal from './editMessageModal/EditMessageModal'

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path='/' component={LoginPage} />
        <Route exact path='/chat' component={Chat} />
        <Route path='/chat/edit/:id' component={EditMessageModal} />
        <Route exact path="/adminPage" component={UserList} />
        <Route exact path="/adminPage/user" component={UserPage} />
        <Route path="/adminPage/user/:id" component={UserPage} />
      </Switch>
      </div>
  );
}

export default App;
